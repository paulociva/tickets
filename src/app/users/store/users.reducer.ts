import { createReducer, on } from '@ngrx/store';

import * as UserActions from './users.actions';
import { User } from '../user';

export const userFeatureKey = 'users';

export interface UserState {
  users: User[];
}

const initialUserState: UserState = {
  users: [],
};

export const userReducers = createReducer(
  initialUserState,
  on(UserActions.getUsersSuccess, (state, {users}) => ({
    ...state,
    users
  }))
);
