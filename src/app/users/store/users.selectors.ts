import { createFeatureSelector, createSelector } from '@ngrx/store';

import { userFeatureKey, UserState } from './users.reducer';

export const selectUserFeature = createFeatureSelector<UserState>(userFeatureKey)

export const selectUsers = createSelector(
  selectUserFeature,
  (state) => state.users
);
