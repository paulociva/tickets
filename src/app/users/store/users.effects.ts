import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';

import { BackendService } from 'src/app/backend.service';
import * as UserActions from './users.actions';

@Injectable()
export class UserEffects {
  fetchUsers$ = createEffect(() =>
    this._actions$.pipe(
      ofType(UserActions.getUsers),
      switchMap(() => this._backendService.users()),
      map((users) => UserActions.getUsersSuccess({users})
      )
    )
  );

  constructor(
    private _actions$: Actions,
    private _backendService: BackendService
  ) {
  }
}
