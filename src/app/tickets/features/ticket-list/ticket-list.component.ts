import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { TicketState } from '../../store/tickets.reducer';
import { getTickets } from '../../store/tickets.actions';
import { selectTicketsArray } from '../../store/tickets.selectors';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html'
})
export class TicketListComponent implements OnInit {
  tickets$ = this._store.select(selectTicketsArray);

  constructor(
    private _store: Store<TicketState>,
  ) {
  }

  ngOnInit(): void {
    this._store.dispatch(getTickets());
  }

}
