import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { TicketState } from '../../store/tickets.reducer';
import { saveTicket } from '../../store/tickets.actions';

@Component({
  selector: 'app-ticket-form',
  templateUrl: './ticket-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TicketFormComponent {
  form: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private _store: Store<TicketState>,
  ) {
    this.form = this._formBuilder.group({
      description: ['', Validators.required]
    })
  }

  save() {
    this._store.dispatch(saveTicket(this.form.getRawValue()));
  }
}
