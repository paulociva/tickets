import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { TicketState } from '../../store/tickets.reducer';
import { completeTicket, selectTicket } from '../../store/tickets.actions';
import { selectTicketSelected } from '../../store/tickets.selectors';
import { selectUsers } from '../../../users/store/users.selectors';
import { getUsers } from '../../../users/store/users.actions';


@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html'
})
export class TicketDetailComponent implements OnInit {
  ticket$ = this._store.select(selectTicketSelected);
  users$ = this._store.select(selectUsers);

  constructor(
    private _store: Store<TicketState>,
    private _activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    const id = this._activatedRoute.snapshot.paramMap.get('id')
    this._store.dispatch(selectTicket({id}))
    this._store.dispatch(getUsers())
  }

  complete(id: number) {
    this._store.dispatch(completeTicket({id}))
  }
}
