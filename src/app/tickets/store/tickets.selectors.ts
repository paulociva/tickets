import { createFeatureSelector, createSelector } from '@ngrx/store';

import { ticketAdapter, ticketFeatureKey, TicketState } from './tickets.reducer';

const {selectEntities, selectAll} = ticketAdapter.getSelectors();

const selectTicketState = createFeatureSelector<TicketState>(ticketFeatureKey);
const getSelectedTicketId = (state: TicketState) => state.selectedTicketId

export const selectTicketSelectedId = createSelector(
  selectTicketState,
  getSelectedTicketId
);
export const selectTicketsEntities = createSelector(
  selectTicketState,
  selectEntities
);
export const selectTicketsArray = createSelector(
  selectTicketState,
  selectAll
);
export const selectTicketSelected = createSelector(
  selectTicketsEntities,
  selectTicketSelectedId,
  (ticketEntities, ticketId) => {
    return ticketId && ticketEntities[ticketId]
  }
);
