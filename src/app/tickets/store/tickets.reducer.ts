import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

import * as TicketActions from './tickets.actions';
import { Ticket } from '../ticket';

export const ticketFeatureKey = 'tickets';
export const ticketAdapter: EntityAdapter<Ticket> = createEntityAdapter<Ticket>();

export interface TicketState extends EntityState<Ticket> {
  selectedTicketId: string | null;
}

const initialState: TicketState = ticketAdapter.getInitialState({
  selectedTicketId: null,
});

export const ticketReducers = createReducer(
  initialState,
  on(TicketActions.getTicketsSuccess, (state, {tickets}) => {
    return ticketAdapter.setMany(tickets, state);
  }),
  on(TicketActions.selectTicket, (state, {id}) => ({
    ...state, selectedTicketId: id
  })),
  on(TicketActions.selectTicketSuccess, TicketActions.saveTicketSuccess, TicketActions.completeTicketSuccess, (state, {ticket}) => {
    return ticketAdapter.upsertOne(ticket, state);
  }),
);
