import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { filter, map, switchMap } from 'rxjs/operators';

import { BackendService } from 'src/app/backend.service';
import * as TicketActions from './tickets.actions';
import { Store } from '@ngrx/store';
import { selectTicketsEntities } from './tickets.selectors';

@Injectable()
export class TicketEffects {
  getTickets$ = createEffect(() =>
    this._actions$.pipe(
      ofType(TicketActions.getTickets),
      switchMap(() => this._backendService.tickets()),
      map((tickets) => TicketActions.getTicketsSuccess({tickets}))
    )
  );

  selectTicket$ = createEffect(() =>
    this._actions$.pipe(
      ofType(TicketActions.selectTicket),
      concatLatestFrom(() => this._store.select(selectTicketsEntities)),
      filter(([, data]) => !!data),
      switchMap(([{id}]) => this._backendService.ticket(Number(id))),
      map((ticket) => TicketActions.selectTicketSuccess({ticket}))
    )
  );

  saveTicket$ = createEffect(() =>
    this._actions$.pipe(
      ofType(TicketActions.saveTicket),
      switchMap(({description}) => this._backendService.newTicket({description})),
      map((ticket) => TicketActions.saveTicketSuccess({ticket})),
    )
  );


  completeTicket$ = createEffect(() =>
    this._actions$.pipe(
      ofType(TicketActions.completeTicket),
      switchMap(({id}) => this._backendService.complete(id, true)),
      map((ticket) => TicketActions.completeTicketSuccess({ticket})),
    )
  );

  constructor(
    private _actions$: Actions,
    private _backendService: BackendService,
    private _store: Store,
  ) {
  }
}
