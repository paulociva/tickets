import { createAction, props } from '@ngrx/store';
import { Ticket } from '../ticket';

export const getTickets = createAction(
  '[Tickets] Get Tickets'
);

export const getTicketsSuccess = createAction(
  '[Tickets] Get Tickets Success',
  props<{ tickets: Ticket[] }>()
);

export const selectTicket = createAction(
  '[Tickets] Select Ticket',
  props<{ id: string }>()
);

export const selectTicketSuccess = createAction(
  '[Tickets] Select Ticket Success',
  props<{ ticket: Ticket }>()
);

export const saveTicket = createAction(
  '[Tickets] Save Ticket',
  props<{ description: string }>()
);

export const saveTicketSuccess = createAction(
  '[Tickets] Save Ticket Success',
  props<{ ticket: Ticket }>()
);

export const completeTicket = createAction(
  '[Tickets] Complete Ticket',
  props<{ id: number }>()
);

export const completeTicketSuccess = createAction(
  '[Tickets] Complete Ticket Success',
  props<{ ticket: Ticket }>()
);
