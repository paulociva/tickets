import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ReactiveFormsModule } from '@angular/forms';

import { ticketFeatureKey, ticketReducers } from './store/tickets.reducer';
import { TicketEffects } from './store/tickets.effects';
import { TicketListComponent } from './features/ticket-list/ticket-list.component';
import { TicketDetailComponent } from './features/ticket-detail/ticket-detail.component';
import { TicketFormComponent } from './features/ticket-form/ticket-form.component';


@NgModule({
  declarations: [TicketListComponent, TicketDetailComponent, TicketFormComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature(ticketFeatureKey, ticketReducers),
    EffectsModule.forFeature([TicketEffects]),
    RouterModule.forChild([
      {
        path: '',
        component: TicketListComponent,
      },
      {
        path: ':id',
        component: TicketDetailComponent,
      },
    ]),
    ReactiveFormsModule
  ],
})
export class TicketsModule {
}
